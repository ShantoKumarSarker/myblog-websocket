var io = require('socket.io')(5000);

console.log('socket server started on 5000');

io.on('connection', function (socket) {
    socket.on('notification', function (data) {
        socket.broadcast.emit('notification', data);
    });
});
