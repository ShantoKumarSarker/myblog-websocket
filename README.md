This project was bootstrapped with Realtime application framework (Node.JS server) [socket.io](https://github.com/socketio/socket.io).

## Available Scripts and steps to run this app

### `git clone https://ShantoKumarSarker@bitbucket.org/ShantoKumarSarker/myblog-websocket.git`

### `cd project-name` in any terminal

### `npm install`

once this installation process is complete then run:

### `node index.js`

Now in terminal you can see msg that this app is live at : http://localhost:5000 and all of msg transfered to socket client will appear here

**Note: Please don't close this terminal !**

